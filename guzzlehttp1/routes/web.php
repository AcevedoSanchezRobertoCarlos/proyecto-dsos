<?php

//use  GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 

Route::get('/', 'PostsController@index');
Route::get('posts/{id}', 'PostsController@show'); //redirige a el postcontroller con la vista show
/*Route::get('/', function () {
    $client  =  new  Client ([ 
	    // El URI base se usa con solicitudes relativas 
	    'base_uri'  =>  'https://jsonplaceholder.typicode.com' , 
	    // Puede establecer cualquier número de opciones de solicitud predeterminadas. 
	    'timeout'   =>  2.0 , 
	]);

	$response = $client->request('GET', 'posts');

	$posts = json_decode($response->getBody()->getContents());

	//dd($response->getBody()->getContents()); // aqui para mostrar el codigo en json

    return view('posts.index', compact('posts'));
});*/
